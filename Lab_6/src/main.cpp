#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//Prototypes
void Init();
void WriteByte(unsigned char DATA);
void WriteData(unsigned char addr, unsigned char data, unsigned char data2);
void InitLEDMatrix(void);
void make_line();
void blink_cursor();

//LED matrix pins
const int CLK = PH4; // Pin 7 //
const int CS  = PH5; // Pin 8 //
const int DIN = PH6; // Pin 9 //

unsigned char LED_map[9][2]={};   //8 addresses, each with 2 bytes
unsigned char cursor[2] = {1, 0}; //cursor[0] == addr, cursor[1] == bit

int main()
{
  Init();

  while(1)
  {
    blink_cursor();
  }
  return 1;
}

void blink_cursor()
{
  char mask = 0x1;

  _delay_ms(1000);

  if(cursor[1] < 8) //if on first LED matrix, 
  {
    mask = 0x1 << cursor[1];        //set mask to cursor bit
    LED_map[cursor[0]][0] ^= mask;  //toggle cursor bit
  } 
  else  //if on second LED matrix
  {
    mask = 0x1 << (cursor[1] - 8);  //set mask to cursor bit
    LED_map[cursor[0]][1] ^= mask;  //toggle cursor bit
  }
  WriteData(cursor[0], LED_map[cursor[0]][0], LED_map[cursor[0]][1]);
}

void make_line()
{
  char mask = 0x1;

  if(cursor[1] < 8) //if on first LED matrix, 
  {
    mask = 0x1 << cursor[1];        //set mask to cursor bit
    LED_map[cursor[0]][0] |= mask;  //set bit
  } 
  else  //if on second LED matrix
  {
    mask = 0x1 << (cursor[1] - 8);  //set mask to cursor bit
    LED_map[cursor[0]][1] |= mask;  //set bit
  }
  WriteData(cursor[0], LED_map[cursor[0]][0], LED_map[cursor[0]][1]);
}

//move up address
ISR(INT0_vect)
{
  make_line();
  if(cursor[0] < 8)   //ensure cursor is in bounds
    cursor[0] +=1;
  else
    cursor[0] = 8;
}

//move  left bit
ISR(INT1_vect)
{
  make_line();
  if(cursor[1] > 0)   //ensure cursor is in bounds
    cursor[1] -=1;
  else
    cursor[1] = 0;
}

//move right bit
ISR(INT2_vect)
{
  make_line();
  if(cursor[1] < 15)  //ensure cursor is in bounds
    cursor[1] +=1;
  else
    cursor[1] = 15;
}

//move down address
ISR(INT3_vect)
{
  make_line();
  if(cursor[0] > 1) //ensure cursor is in bounds
    cursor[0] -=1;
  else
    cursor[0] = 1;
}

//clear the matrices
ISR(INT4_vect)
{
  //clear the map and write it
  for(int i=0; i<9; i++)
  {
    LED_map[i][0] = 0;
    LED_map[i][1] = 0;
    WriteData(i, LED_map[i][0], LED_map[i][1]);
  } 
}

void Init()
{
  //set PORTH for LED matrix output
  DDRH = 0b01110000;

  //set PORTB for output
  DDRB = 0xFF;
  //Generate a software interrupt on external pins D3:0
  //set PORTD 3:0 as output (int pins)
  DDRD = 0x0F;
  DDRE = 0x10;
  //Set PORTB, PORTD, and PORTE to known states
  PORTB = 0xFF;
  PORTD = 0x00;
  PORTE = 0x00;

  //Initialize External Interrupts on PORTD3:0
  sei();  //enable global interrupt enable flag so that interrupts can be processed
  //set each interrupt in portd3:0 to be triggered on a rising edge
  EICRA = 0xFF;
  EICRB = 0x03; //set porte4 to trigger
  //enable interrupt on PORTD0
  EIMSK |= 0x1F;

  InitLEDMatrix();
}

//setup initial settings and cycle test pattern
void InitLEDMatrix(void)
{
  WriteData(0x90, 0x00, 0x00);  //decoding//
  WriteData(0x0a, 0x08, 0x08);  //brightness//
  WriteData(0x0b, 0x07, 0x07);  //scan limits//
  WriteData(0x0c, 0x01, 0x01);  //power down mode//
  WriteData(0x0f, 0x01, 0x01);  //test display//
  _delay_ms(100);
  WriteData(0x0f, 0x00, 0x00);  //test display//
  _delay_ms(100);
  WriteData(0x0f, 0x01, 0x01);  //test display//
  _delay_ms(100);
  WriteData(0x0f, 0x00, 0x00);  //test display//
  for(int i = 1; i<9; i++)
    WriteData(i, 0x00, 0x00);  //clear display//
}

//writes a single byte out to PH6
void WriteByte(unsigned char DATA)
{
  for(int i=7; i>=0; i--)
  {
    PORTH &= 0b11101111;                //clear CLK
    PORTH &= 0b10111111;                //clear PH6
    PORTH |= (((DATA >> i) & 1) << 6);  //load bit to PH6
    PORTH |= 0b00010000;                //set CLK
  }
}

//enable CS, write address and then data to PH6
void WriteData(unsigned char addr, unsigned char data, unsigned char data2)
{
  
  PORTH &= 0b11011111;  //clear CS
  WriteByte(addr);
  WriteByte(data);
  _delay_ms(25);
  WriteByte(addr);
  WriteByte(data2);
  PORTH |= 0b00100000;  //set CS
}