#include <avr/io.h>
#include <avr/delay.h>

//function declarations
void Init();
void clearDisplay();
void updateBit(int x, int y, int RGB);
void writeDisplay(int brightness);

int data[16][64] = {0};

enum color{red=0b00100100, blue=0b10010000, green=0b01001000, yellow=0b01101100, cyan=0b11011000, pink=0b10110100, white=0b11111100, off=0}; //for LED colors


#define CLOCK = 11; //PB5
#define LATCH = 10; //PB4
#define OE = 9;     //PH6

/*
    R1 = 24 PA2
    G1 = 25 PA3
    B1 = 26 PA4
    R2 = 27 PA5
    G2 = 28 PA6
    B2 = 29 PA7
*/

int main()
{
  Init();
  bool layout_test = false; //for color testing only
  int data2[64] = {0};

  //set all values in data to pink
  //for(int i=0;i<16; i++)
  //{
  //  for(int j=0; j<64; j++)
  //    data[i][j] = pink & 0b11111100;
  //}
 
  //case 1 outputs color gradients, case 2 draws spaced out columns
  if(layout_test)
  {
    for(int i=0; i<32; i++)
    {
      data2[i] = (i << 5) | i;
      data2[i+32] = (i << 5) | i;
    }
  }
  else
  {
    for(int i=0; i<32; i++)
    {
      if(i==7) //red?
      {
        data2[i] = 0b00100100;
        data2[i+32] = 0b00100100;
      }
      else if(i==15) //green?
      {
        data2[i] = 0b01001000;
        data2[i+32] = 0b01001000;
      }
      else if(i==23) //blue?
      {
        data2[i] = 0b10010000;
        data2[i+32] = 0b10010000;
      }
      else if(i==31) //white?
      {
        data2[i] = 0b11111100;
        data2[i+32] = 0b11111100;
      }
    }
  }


  _delay_us(100);

  for(int i=5; i<=20; i++)
  {
    updateBit(5, i, cyan);
    updateBit(15, i, cyan);
    updateBit(20, i, cyan);
    updateBit(30, i, cyan);
    updateBit(40, i, cyan);
    updateBit(50, i, cyan);
    updateBit(58, i, cyan);
  }
  for(int i=1; i<6; i++)
  {
    updateBit(i+5, 10, cyan);
    updateBit(i+10, 10, cyan);
    updateBit(i+20, 5, cyan);
    updateBit(i+20, 20, cyan);
    updateBit(i+20, 10, cyan);
    updateBit(i+30, 20, cyan);
    updateBit(i+40, 20, cyan);
    updateBit(i+50, 5, cyan);
    updateBit(i+50, 20, cyan);
    updateBit(i+53, 5, cyan);
    updateBit(i+53, 20, cyan);
  }
  

  int x = 10;
  int y = 24;
  int y2 = 0;
  bool direction = 1;
  while(1)
  {
    for(int i=0; i<2; i++)
      writeDisplay(50);
    updateBit(x, y, off);
    updateBit(2, y2, off);
    if(direction)
    {
      x++;
      y2++;
    }
    else
    {
      x--;
      y2--;
    }
    updateBit(x, y, red);
    updateBit(2, y2, cyan);
    if(x>40 || x<=10)
      direction = !direction;
  }

  return 1;
}

void writeDisplay(int brightness)
{
  for(int j=0; j<16; j++)
  {
    PORTF = j;

    for(int i=0; i<64; i++)
    {
      //data2 for color tests, data for full array test
      //PORTA = data2[i-1];
      PORTA = data[j][i];
      
      //can dim display, but tends to cause flicker above 10us
      //clearDisplay();
      //_delay_us(10);
      //PORTH = 0x00;
      PORTB |= 0b00100000;                //set CLK
      _delay_us(1);
      PORTB &= 0b11011111;                //clear CLK
      _delay_us(1);
    }

    //much above 10ms it starts to noticibly flicker
    clearDisplay();
    for(int k=100; k>brightness; k--)
      _delay_us(10);
    PORTH = 0x00;


    PORTB |= 0b00010000;  //Set latch
    PORTB &= 0b11101111;  //clear latch
  }
  
}
//give x and y coordinate, and then RGB=1 for red, RGB=2 for green, RGB=4 for blue
void updateBit(int x, int y, int RGB)
{
  int temp_color = 0;
  if(y < 16)
  {
    RGB &= 0b00011100;                      //keep only lower RGB bits
    if(y<2)
    {
      temp_color = data[y+14][x] & 0b11100011;
      data[y+14][x] = temp_color | RGB;
    }
    else
    {
      temp_color = data[y-2][x] & 0b11100011;   //clear lower RGB bits
      data[y-2][x] = temp_color | RGB;          //or new value into lower RGB bits
    }
  }
  else
  {
    RGB &= 0b11100000;                      //keep only upper RGB bits
    if(y<18)
    {
      temp_color = data[y-2][x] & 0b00011111;   //clear higher RGB bits
      data[y-2][x] = temp_color | RGB;
    }
    else
    {
      temp_color = data[y-18][x] & 0b00011111;
      data[y-18][x] = temp_color | RGB;       //or new value into higher RGB bits

    }
  }
}


void clearDisplay()
{
  PORTH = 0xFF;
  /*
  for(int j=0; j<17; j++)
    {
      PORTF = j;
      PORTA = 0x00;   //Set the rgb values

      for(int i=0; i<384; i++)
      {
        //PORTB &= 0b11011111;                //clear CLK
        //PORTA = 0xAA;   //Set the rgb values
        _delay_us(1);
        PORTB |= 0b00100000;                //set CLK
        _delay_us(1);
        PORTB &= 0b11011111;                //clear CLK
        //PORTA = 0xFF;
        //_delay_us(100);
      }

      PORTB |= 0b00010000;  //Set latch
      _delay_us(100);
      PORTB &= 0b11101111;  //clear latch
    }
    */
}

void Init()
{
  DDRA = 0xFF;
  DDRB = 0xFF;
  DDRH = 0xFF;
  DDRF = 0xFF;

  PORTA = 0x00;
  PORTB = 0x00;
  PORTH = 0x00;
  PORTF = 0x00;
}

