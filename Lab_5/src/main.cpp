#include <avr/io.h>
#include <util/delay.h>

//Helpful LCD control defines
#define LCD_Reset             0b00110000  //reset LCD to put in 4-bit mode
#define LCD_4bit_enable       0b00100000  //4-bit data - can't set the line display or fonts until this is set
#define LCD_4bit_mode         0b00101000  //2-line display, 5x8 font
#define LCD_4bit_displayOFF    0b00001000  //set display off
#define LCD_4bit_displayON     0b00001100  //set display on - no blink
#define LCD_4bit_displayON_B1  0b00001101  //set display on - with blink
#define LCD_4bit_displayCLEAR  0b00000001  //replace all chars with "space"
#define LCD_4bit_entryMODE     0b00000110  //set curser to write/read from left -> right
#define LCD_4bit_cursorSET     0b10000000  //set cursor position

//For two line mode
#define LineOneStart 0x00
#define LineTwoStart 0x40

//Baud stuff
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE F_CPU / (USART_BAUDRATE*16UL)-1

// Pin definitions for PORTB control lines
#define LCD_EnablePin 1
#define LCD_RegisterSelectPin 0

void LCD_init(void);
void LCD_E_RS_init();
void LCD_write_4bits(uint8_t);
void LCD_EnablePulse(void);
void LCD_write_instruction(uint8_t);
void InitUSART(void);
void LCD_write_char(char);
char ReadBit();
char WriteMenu();
void NewLine(uint16_t line);
void PrintChar();
void PrintString(char* string);
void PrintInput();
void ScrollText(char* input, int size);
void LCD_write_line(char *line);


void LCD_init()
{
  //wait for power up - more than 30ms for vdd to rise to 4.5V
  _delay_ms(100);

  //we need to reset controller to enable 4-bit mode
  LCD_E_RS_init();  //Set the E and RS pins active low for each LCD reset

  //reset and wait for activiation
  LCD_write_4bits(LCD_Reset);
  _delay_ms(10);

  //now we can set the LCD to 4-bit mode
  LCD_write_4bits(LCD_4bit_enable);
  _delay_us(80);

  //set up LCD modes
  /*
  At this point we are operating in 4-bit mode
  (which means we have to send the high-nibble and low-nibble separate)
  and can now set the line numbers and font size
  */
  LCD_write_instruction(LCD_4bit_mode);
  _delay_us(80);  //delay must be > 39us

  LCD_write_instruction(LCD_4bit_displayCLEAR);
  _delay_ms(80);  //must be > 1.53ms

  LCD_write_instruction(LCD_4bit_entryMODE);
  _delay_us(80);  //must be > 39us

  LCD_write_instruction(LCD_4bit_displayON);
  _delay_us(80);  //must be > 39us
}

void LCD_E_RS_init()
{
  //set up the E and RS lines to active low for the reset function
  PORTB &= -(1<<LCD_EnablePin);
  PORTB &= -(1<<LCD_RegisterSelectPin);
}

void LCD_write_4bits(uint8_t Data)
{
  //only sends the data to the upper 4 bits of PORTA
  PORTA &= 0b00001111;  //clear upper nybble of PORTA
  PORTA |= Data;        //write data to data lines on PORTA

  //need to pulse enable to send data
  LCD_EnablePulse(); 
}

void LCD_write_instruction(uint8_t Instruction)
{
  //ensure RS is low
  //PORTB &= -(1<<LCD_RegisterSelectPin);
  LCD_E_RS_init();

  LCD_write_4bits(Instruction);   //write high nybble
  LCD_write_4bits(Instruction<<4);//write low nybble
}

// Pulse the Enable pin on the LCD controller to write/read the data lines - should be a t least 230ns pulse width //
void LCD_EnablePulse(void)
{
    // Set the enable bit low -> high -> low //
    //PORTB &= ~(1<<LCD_EnablePin); //Set enable low //
    //_delay_us(1); // what to ensure the pin is low //
    PORTB |= (1<<LCD_EnablePin); //Set enable high //
    _delay_us(1); // wait to ensure the pin is high //
    PORTB &= ~(1<<LCD_EnablePin); // Set enable low //
    _delay_us(1); // wait to ensure the pin is low //
}

void LCD_write_char(char Data)
{
    // Set up the E and RS lines for data writing //
    PORTB |= (1<<LCD_RegisterSelectPin); // Ensure RS pin is set high //
    PORTB &= ~(1<<LCD_EnablePin); // Ensure the enable pin is low //
    LCD_write_4bits(Data); //write the upper nybble //
    LCD_write_4bits(Data<<4); // write the lower nybble //
    _delay_us(80); // need to wait > 43us //
}

int main(void)
{
    DDRB = 0x23;
    DDRA = 0xF0;
    char MyChar;

    //Initialize the LCD for 4-bit mode, two lines, and 5 x 8 dots //
    // Inits found on Page 26 of Datasheet and Table 7 for function set intstructions //
    LCD_init();
    InitUSART();
    char scrollingText[26] = "This is scrolling text!!!";
    char string[17] =     " Hello World!   ";
    char invalidArg[17] = " Invalid Input  ";

    while(1)
    {
        LCD_write_instruction(LCD_4bit_displayCLEAR);
        _delay_us(10);
        NewLine(LineOneStart);
      MyChar = WriteMenu();
      switch(MyChar) {
        case '1':
          PrintChar();
          break;
        case '2':
          PrintString(string);
          break;
        case '3':
          PrintInput();
          break;
        case '4':
          ScrollText(scrollingText, 26);
          break;
        default:
          LCD_write_instruction(LCD_4bit_displayCLEAR);
          _delay_us(10);
          NewLine(LineOneStart);
          PrintString(invalidArg);
          break;
        }
        _delay_ms(1000);
    }
    return 1;
}

void PrintChar()
{
  char MyChar;
  char prompt[32] = " Enter a single char to display";

  LCD_write_instruction(LCD_4bit_displayCLEAR);
  _delay_us(10);
  NewLine(LineOneStart);
  for(int i=0; i<15; i++)
    LCD_write_char(prompt[i]);
  _delay_us(10);
  NewLine(LineTwoStart);
  _delay_us(10);
  for(int i=15; i<32; i++)
    LCD_write_char(prompt[i]);
  
  MyChar = ReadBit();
  LCD_write_instruction(LCD_4bit_displayCLEAR);
  _delay_us(10);
  NewLine(LineOneStart);
  LCD_write_char(' ');
  _delay_us(10);
  LCD_write_char(MyChar);
}

void PrintString(char* string)
{
  LCD_write_instruction(LCD_4bit_displayCLEAR);
  NewLine(LineOneStart);
  LCD_write_line(string);
  return;
}

void PrintInput()
{
  char MyChar;
  bool pickLine = false;

  LCD_write_instruction(LCD_4bit_displayCLEAR);
  NewLine(LineOneStart);
  while(1)
  {
    MyChar = ReadBit();
    if(MyChar == '\r')
    {
      pickLine = !pickLine;
      if(pickLine)
        NewLine(LineTwoStart);
      else
        NewLine(LineOneStart);
    }
    else if(MyChar == 8)//backspace
    {
      LCD_write_instruction(LCD_4bit_displayCLEAR);
    }
    else if (MyChar != '\n')
    {
      LCD_write_char(MyChar);
    }
  }
}

//Switch to line given and clear
void NewLine(uint16_t line)
{
  LCD_write_instruction(LCD_4bit_cursorSET | line);
  _delay_us(10);
  for(int i=0; i<16; i++)
    LCD_write_char(' ');

  _delay_us(10);
  LCD_write_instruction(LCD_4bit_cursorSET | line);
}

void LCD_write_line(char *line)
{
  for(int i=0; i<16; i++)
    LCD_write_char(line[i]);
}

void ScrollText(char* input, int size)
{
  char window[17] = {};
  int numShifts = size-16; 

  LCD_write_instruction(LCD_4bit_displayCLEAR);
  NewLine(LineOneStart);

  //load and output inital frame
  for(int i=0; i<16; i++)
    window[i+1] = input[i];

  LCD_write_line(window);
  _delay_ms(1000);
  
  //output scrolling text
  for(int i=0; i< numShifts; i++)
  {
    for(int j=0; j<16; j++)
      window[j+1] = input[i+j];

    //output the line
    NewLine(LineOneStart);
    LCD_write_line(window);
    _delay_ms(400);
  }
}


char WriteMenu()
{
  char choice; 
  char LineOne[19] = " 1)char  2)str    ";
  char LineTwo[19] = " 3)input 4)scroll ";

  //output menu prompt
  LCD_write_line(LineOne);
    
  NewLine(LineTwoStart);

  LCD_write_line(LineTwo);
  
  //read in user choice
  choice = ReadBit();

  return choice;
}

char ReadBit()
{
  char Temp;
  while ((UCSR0A & (1<< RXC0)) == 0) {};
  Temp = UDR0;

  return Temp;
}

void InitUSART(void)
{
  //enable RX and TX
  UCSR0B |= 0x18;

  //use 8-bit character frames in asynchrounous mode
  UCSR0C |= 0x06;

  //set the baud rate (upper 4 bits should be zero)
  UBRR0L = BAUD_PRESCALE;
  UBRR0H = (BAUD_PRESCALE >> 8);
}

