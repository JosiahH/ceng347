#include <avr/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>

volatile bool device_on = false;  //set with interrupt
volatile bool goofy_switch = false; //use to cancel bounce on switch interrupt

volatile unsigned int ADCval[2] = {511, 511};
volatile unsigned int IRcount = 0;
volatile unsigned int speed = 3;

void Init();
void stepMotor();
void reverseMotor();
void new_delay(int j);

int main()
{
  //
  Init();  
  unsigned int upper_tolerance = 800;
  unsigned int lower_tolerance = 200;
  unsigned int threshold[4] = {204, 408, 610, 804};

  ADCSRA |= 0x40;
  while(1)
  {
    if(device_on)
    {
      //Power LED to signal on
      PORTB &= not(0x40);

      //check and adjust speed
      if(ADCval[1] < threshold[0])
        speed = 0;
      else if (ADCval[1] < threshold[1])
        speed = 1;
      else if (ADCval[1] < threshold[2])
        speed = 2;
      else if (ADCval[1] < threshold[3])
        speed = 3;
      else if (ADCval[1] >= threshold[3])
        speed = 4;
      

      if(ADCval[0] > upper_tolerance)
        stepMotor();
      else if(ADCval[0] < lower_tolerance)
        reverseMotor();
      //Set LED dimness to match speed setting
      OCR0A = uint8_t((255/6)*(6-speed));
    }
    else
    {
      //turn off LED's
      PORTB |= 0xC0;
      PORTH = 0x00;
      OCR0A = 255;
    }

  }

  return 42;
}

void stepMotor()
{
  PORTH = 0x03 << 3;
  new_delay(speed);
  PORTH = 0x06 << 3;
  new_delay(speed);
  PORTH = 0x0C << 3;
  new_delay(speed);
  PORTH = 0x09 << 3;
  new_delay(speed);
}

void reverseMotor()
{
  PORTH = 0x09 << 3;
  new_delay(speed);
  PORTH = 0x0C << 3;
  new_delay(speed);
  PORTH = 0x06 << 3;
  new_delay(speed);
  PORTH = 0x03 << 3;
  new_delay(speed);
}

void new_delay(int j)
{
  for(int i=6; i>j; i--)
  {
    _delay_ms(2);
  }
}

void Init()
{
  //set PORTB and PORTH for output
  DDRB = 0xFF;
  DDRH = 0xFF;
  //set PORTD 3:0 as input for switch
  DDRD = 0xFF;
  //set PORTF for analog input
  DDRF = 0x00;
  //Set PORTB, PORTD to known states
  PORTB = 0xFF;
  PORTD = 0xFF;
  PORTH = 0xFF;

  //LED FastPWM
  TCCR0A = 0xA3;
  TCCR0B= 0x05;
  TCNT0 = 0x00;
  OCR0B = 192;
  OCR0A = 255;

  //Initialize External Interrupts on PORTD3:0
  //set each interrupt in portd3:0 
  EICRA = 0x0F;
  //enable interrupt on PORTD0
  EIMSK |= 0x0F;

  ADCSRA |= 0x8F;
  //ADCSRB |= 0x8F;
  ADMUX |= 0x40;

  sei();  //enable global interrupt enable flag so that interrupts can be processed
}

//move up address
ISR(INT0_vect)
{
  //toggle LED when interrupt and toggle global bool
  if(goofy_switch)
  {
    device_on = !(device_on);
  }
  goofy_switch = !(goofy_switch);
}

ISR(ADC_vect)
{
  ADCval[IRcount] = ADCW;
  switch(IRcount){
  case 0:
    ADMUX = 0x41;
    IRcount++;
    break;
  case 1:
    ADMUX = 0x40;
    IRcount = 0;
    break;
  default: break;
  }
  
  ADCSRA |= 0x40;
}