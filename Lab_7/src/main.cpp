#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>

//constants
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE F_CPU / ((USART_BAUDRATE*16UL)-1)

//globals
volatile unsigned short T0IntCnt = 0;
volatile unsigned short TotalT0Ints = 125;
volatile char note = ' ';
volatile bool NewNote = false;

//prototypes
void InitUSART(void);
void Init();

struct toneMap
{
  char noteName;
  uint8_t noteTiming;
};

enum keyList { A3=133,  B3=123,  C4=118,  D4=105, E4=93, F4=88, G4=78, A4=70, B4=62, C5=58, rest=255};

struct songMap
{
  uint8_t noteValue;
  uint8_t sustain;
};

struct songMap mySong[64] = {{F4, 1}, {G4, 1}, {F4, 1}, 
                             {E4, 8}, {rest, 1}, {F4, 1}, {A4, 1}, {E4, 1}, 
                             {D4, 2}, {rest, 1}, {D4, 1}, {F4, 1}, {D4, 1},
                             {C4, 4}, {rest, 1}, {D4, 1}, 
                             {A4, 2}, {G4, 2}, {rest, 1}, {D4, 1},
                             {A4, 2}, {G4, 2}, {F4, 2},
                             {D4, 8}, {rest, 1}, {F4, 1}, {G4, 1}, {F4, 1},
                             {E4, 8}, {rest, 1}, {E4, 1}, {G4, 1}, {E4, 1},
                             {C4, 2}, {rest, 1}, {C4, 1}, {E4, 1}, {C4, 1},
                             {A3, 2}, {rest, 1}, {A3, 1}, {C4, 1}, {A3, 1},
                             {rest, 2}, {B3, 2}, {A3, 2},
                             {C4, 3}, {D4, 1}, {E4, 2},
                             {rest, 2}, {C4, 2}, {D4, 2},
                             {E4, 6}, {F4, 1}, {A4, 1}, {F4, 1}, 
                             {E4, 8},
                             {rest, 1}
                             };
//                               A3          B3         C         D           E         F         G         A         B         C
struct toneMap toneList[10] = {{'A', 133},{'B', 123},{'a',118}, {'s',105}, {'d',93}, {'f',88}, {'j',78}, {'k',70}, {'l',62}, {';',58}};

ISR(USART0_RX_vect)
{
  while ((UCSR0A & (1<< RXC0)) == 0) {};
  note = UDR0;
  while((UCSR0A & (1 << UDRE0)) == 0){};
  UDR0 = note;
  
  NewNote = true;   
}

int main()
{
  Init();
  TCCR0A = 0x1F;
  for(int i = 0; i < 64; i++)
  {
    OCR0A = mySong[i].noteValue;
    if(mySong[i].noteValue == 255)
      TCCR0A = 0x1F;
    else
      TCCR0A = 0x5F;
    for(int j = 0; j < mySong[i].sustain; j++)
      _delay_ms(200);
  }

  while(1)
  {  
    
    if(NewNote)
    {
      for(int i=0; i<10; i++)
      {
        if(toneList[i].noteName == note)
        {
          //TCNT0 = 0x00;
          OCR0A = toneList[i].noteTiming;
        }
      }
      TCCR0A = 0x5F;
      NewNote = false;
      _delay_ms(100);
      TCCR0A = 0x1F;
    }
  }

  return 1;
}


void Init()
{
  //set buzzer output
  DDRB = 0xFF;
  DDRG = 0XFF;

  //Set up timer 0
  sei();

  //Fast PWM
  TCCR0A = 0x5F;
  TCCR0B = 0x0C;
  //Initialize TCNT0 to 0
  TCNT0 = 0x00;
  //Set the Output compare A register for compare match to 249
  OCR0A = 130;
  OCR0B = 255;
  //Enable 
  //TIMSK0 = 0x02; //for compare A match
  InitUSART();
}

//initialize tx/rx for transmitting
void InitUSART(void)
{
  //enable RX and TX
  UCSR0B |= 0x98;

  //use 8-bit character frames in asynchrounous mode
  UCSR0C |= 0x06;

  //set the baud rate (upper 4 bits should be zero)
  UBRR0L = BAUD_PRESCALE;
  UBRR0H = (BAUD_PRESCALE >> 8);
}