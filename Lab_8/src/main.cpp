#include <avr/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdlib.h>
#include <ctype.h>

//constants
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE F_CPU / ((USART_BAUDRATE*16UL)-1)
#define MIDDLE 370
#define MAX 620
//#define MAXMAX 655
#define MIN 120
//#define MINMAX 86

//Helpful LCD control defines
#define LCD_Reset             0b00110000  //reset LCD to put in 4-bit mode
#define LCD_4bit_enable       0b00100000  //4-bit data - can't set the line display or fonts until this is set
#define LCD_4bit_mode         0b00101000  //2-line display, 5x8 font
#define LCD_4bit_displayOFF    0b00001000  //set display off
#define LCD_4bit_displayON     0b00001100  //set display on - no blink
#define LCD_4bit_displayON_B1  0b00001101  //set display on - with blink
#define LCD_4bit_displayCLEAR  0b00000001  //replace all chars with "space"
#define LCD_4bit_entryMODE     0b00000110  //set curser to write/read from left -> right
#define LCD_4bit_cursorSET     0b10000000  //set cursor position

//For two line mode
#define LineOneStart 0x00
#define LineTwoStart 0x40

// Pin definitions for PORTB control lines
#define LCD_EnablePin 1
#define LCD_RegisterSelectPin 0

//prototypes
void InitUSART(void);
void Init();
void Init16();
void PrintMenu();
void SerialPrintLine(char* string, int size);
int SetDCSpeed();
int SetServoAngle();
int read_uart();
void LCD_init();
void WagTail();

//LCD prototypes
void LCD_init(void);
void LCD_E_RS_init();
void LCD_write_4bits(uint8_t);
void LCD_EnablePulse(void);
void LCD_write_instruction(uint8_t);
void LCD_write_char(char);
void NewLine(uint16_t line);
void PrintString(char* string);
void LCD_write_line(char *line);


ISR(USART0_RX_vect)
{
}


int main(void)
{
  Init();
  Init16();
  LCD_init();
  bool validEntry = true;
  uint8_t input;
  int servoAngle = 0;
  int DCSpeed = 0;
  char output[2][16] = {" Angle: 0       ", " Speed: 0       "};
  char itoaValue[2][4];
  LCD_write_instruction(LCD_4bit_displayCLEAR);
  _delay_us(10);
  NewLine(LineOneStart);
  LCD_write_line(output[0]);
  NewLine(LineTwoStart);
  LCD_write_line(output[1]);

  while(1)
  {
    if(validEntry)
    {
      PrintMenu();
      validEntry = false;
    } 
    while ((UCSR0A & (1<< RXC0)) == 0) {};
    input = UDR0;
    while((UCSR0A & (1 << UDRE0)) == 0){};
    UDR0 = input;

    //switch input:
    switch (input)
    {
    case '1':
      DCSpeed = SetDCSpeed();
      itoa(DCSpeed, itoaValue[0],10);
      for(int i=1; i<4; i++)
      { 
        if(!isdigit(itoaValue[0][i]))
          itoaValue[0][i] = ' ';
      }
      NewLine(LineTwoStart);
      for(int i=0; i<4; i++)
        output[1][i+8] = itoaValue[0][i];
      LCD_write_line(output[1]);
      validEntry = true;
      break;
    case '2':
      servoAngle = SetServoAngle();
      itoa(servoAngle, itoaValue[1],10);
      for(int i=1; i<4; i++)
      { 
        if(!isdigit(itoaValue[1][i]))
          itoaValue[1][i] = ' ';
      }
      NewLine(LineOneStart);
      for(int i=0; i<4; i++)
        output[0][i+8] = itoaValue[1][i];
      LCD_write_line(output[0]);
      validEntry = true;
      break;
    case '3':
      WagTail();
      break;
    default:
      SerialPrintLine("Invalid Entry!", 15);
    }


  }

  return 1;
}

void WagTail()
{
  for(int i=0; i<5; i++)
  {
    OCR1B = 400;
    _delay_ms(200);
    OCR1B = MIDDLE;
    _delay_ms(200);
    OCR1B = 200;
    _delay_ms(200);
  }
}

int SetDCSpeed()
{
  int input; 
  SerialPrintLine("Enter value for speed (10-100%): ", 33);
  
  input = read_uart();
  OCR0A = uint8_t((input * 255) / 100);

  return input;
}

int SetServoAngle()
{
  int input;
  int normalizedAngle = 0;
  int range = MAX - MIN;
  uint16_t result;
  float scale = range / 180.0f;

  SerialPrintLine("Enter value for servo angle (-90 - 90): ", 41);
  input = read_uart();

  normalizedAngle = -input + 90;
  result = uint16_t(normalizedAngle * scale) + uint16_t(MIN);

  if(result < MIN)
    result = MIN;
  else if(result > MAX)
    result = MAX;
  OCR1B = result;

  return input;
}

void PrintMenu()
{
  char line1[51] = "Please select the motor you would like to control:";
  char line2[12] = "1) DC Speed";
  char line3[15] = "2) Servo Angle";

  //print line 1
  SerialPrintLine(line1, 51);

  //print line 2
  SerialPrintLine(line2, 12);

  //print line 3
  SerialPrintLine(line3, 15);
}

void SerialPrintLine(char* string, int size)
{
  for(int i=0; i<size-1; i++)
  {
    while((UCSR0A & (1 << UDRE0)) == 0){};
    UDR0 = string[i];
  }
  while((UCSR0A & (1 << UDRE0)) == 0){};
  UDR0 = '\n';
}

void Init()
{
  DDRA = 0xF0;
  DDRB = 0xFF;
  PORTB = 0xFF;
  DDRD = 0xFF;

  sei();

  TCCR0A = 0xA3;
  TCCR0B= 0x05;

  TCNT0 = 0x00;

  OCR0B = 192;
  OCR0A = 0;
  InitUSART();
}

void Init16()
{
  //initialize PWM
  TCCR1A = 0xAB;
  TCCR1B = 0x1B;
  TCNT1 = 0x0000;

  //set to 20ms cycle and 1.5ms 
  OCR1A = 5000;
  OCR1B = MIDDLE
;
}


//initialize tx/rx for transmitting
void InitUSART(void)
{
  //enable RX and TX
  UCSR0B |= 0x98;

  //use 8-bit character frames in asynchrounous mode
  UCSR0C |= 0x06;

  //set the baud rate (upper 4 bits should be zero)
  UBRR0L = BAUD_PRESCALE;
  UBRR0H = (BAUD_PRESCALE >> 8);
}

int read_uart()
{
  char input;
  char string[7];

  //shouldn't be any input longer than 3
  for(int i=0;i<7; i++)
  {
    while ((UCSR0A & (1<< RXC0)) == 0) {};
      input = UDR0;
    while((UCSR0A & (1 << UDRE0)) == 0){};
    UDR0 = input;

    //if \n, end loop with a null char
    if(input == '\n')
    {
      string[i] = '\0';
      i=100;
    }
    else 
      string[i] = input;
  }
  //convert to int and return
  return atoi(string);
}

void LCD_init()
{
  //wait for power up - more than 30ms for vdd to rise to 4.5V
  _delay_ms(100);

  //we need to reset controller to enable 4-bit mode
  LCD_E_RS_init();  //Set the E and RS pins active low for each LCD reset

  //reset and wait for activiation
  LCD_write_4bits(LCD_Reset);
  _delay_ms(10);

  //now we can set the LCD to 4-bit mode
  LCD_write_4bits(LCD_4bit_enable);
  _delay_us(80);

  //set up LCD modes
  /*
  At this point we are operating in 4-bit mode
  (which means we have to send the high-nibble and low-nibble separate)
  and can now set the line numbers and font size
  */
  LCD_write_instruction(LCD_4bit_mode);
  _delay_us(80);  //delay must be > 39us

  LCD_write_instruction(LCD_4bit_displayCLEAR);
  _delay_ms(80);  //must be > 1.53ms

  LCD_write_instruction(LCD_4bit_entryMODE);
  _delay_us(80);  //must be > 39us

  LCD_write_instruction(LCD_4bit_displayON);
  _delay_us(80);  //must be > 39us
}


void LCD_E_RS_init()
{
  //set up the E and RS lines to active low for the reset function
  PORTB &= -(1<<LCD_EnablePin);
  PORTB &= -(1<<LCD_RegisterSelectPin);
}

void LCD_write_4bits(uint8_t Data)
{
  //only sends the data to the upper 4 bits of PORTA
  PORTA &= 0b00001111;  //clear upper nybble of PORTA
  PORTA |= Data;        //write data to data lines on PORTA

  //need to pulse enable to send data
  LCD_EnablePulse(); 
}

void LCD_write_instruction(uint8_t Instruction)
{
  //ensure RS is low
  //PORTB &= -(1<<LCD_RegisterSelectPin);
  LCD_E_RS_init();

  LCD_write_4bits(Instruction);   //write high nybble
  LCD_write_4bits(Instruction<<4);//write low nybble
}

// Pulse the Enable pin on the LCD controller to write/read the data lines - should be a t least 230ns pulse width //
void LCD_EnablePulse(void)
{
    // Set the enable bit low -> high -> low //
    //PORTB &= ~(1<<LCD_EnablePin); //Set enable low //
    //_delay_us(1); // what to ensure the pin is low //
    PORTB |= (1<<LCD_EnablePin); //Set enable high //
    _delay_us(1); // wait to ensure the pin is high //
    PORTB &= ~(1<<LCD_EnablePin); // Set enable low //
    _delay_us(1); // wait to ensure the pin is low //
}

void LCD_write_char(char Data)
{
    // Set up the E and RS lines for data writing //
    PORTB |= (1<<LCD_RegisterSelectPin); // Ensure RS pin is set high //
    PORTB &= ~(1<<LCD_EnablePin); // Ensure the enable pin is low //
    LCD_write_4bits(Data); //write the upper nybble //
    LCD_write_4bits(Data<<4); // write the lower nybble //
    _delay_us(80); // need to wait > 43us //
}

void PrintString(char* string)
{
  LCD_write_instruction(LCD_4bit_displayCLEAR);
  NewLine(LineOneStart);
  LCD_write_line(string);
  return;
}

//Switch to line given and clear
void NewLine(uint16_t line)
{
  LCD_write_instruction(LCD_4bit_cursorSET | line);
  _delay_us(10);
  for(int i=0; i<16; i++)
    LCD_write_char(' ');

  _delay_us(10);
  LCD_write_instruction(LCD_4bit_cursorSET | line);
}

void LCD_write_line(char *line)
{
  for(int i=0; i<16; i++)
    LCD_write_char(line[i]);
}