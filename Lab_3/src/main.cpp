extern "C" {
  void start();
  void increment();
}

void(init()){
  start();
}

int main()
{
  init();
  while(1)
  {
    increment();
  }
  return 1;
}